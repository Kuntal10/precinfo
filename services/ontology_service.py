from owlready2 import *
import json
from oao.oao import Oao
from oao.pizza_oao import PizzaOao

class OntologyService():
    def __init__(self, onto_id):
        print('Loading ontology...')
        self.onto_id = onto_id
        self.world = World()
        self.onto = self.world.get_ontology('repository/' + self.onto_id + '_onto.owl').load()
        self.oao = Oao(self)
        self.pizza_oao = PizzaOao(self)
        print('Loaded.')
        
    def get_onto(self):
        return self.onto

    def get_world(self):
        return self.world

    def sync(self):
        with self.onto:
            print('starting reasoner...')
            sync_reasoner(self.world, debug=1)  
            print('synced')

    def save(self): 
        with self.onto:
            onto_path.append(os.path.dirname(os.path.abspath(__file__)) + '../repository')
            self.onto.save()
            print('saved')