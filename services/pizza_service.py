from owlready2 import *
from oao.oao import Oao
from pprint import pprint
import json

class PizzaService:
    def __init__(self, ontology_service): 
        self.ontology_service = ontology_service
        self.onto = self.ontology_service.get_onto()
        self.oao = self.ontology_service.oao
        

    def create_pizza(self, onto_pizza, data):
        with self.onto:
            for t in data.toppings:
                print(t)
                t_individual = self.oao.get_topping_instance(t)
                print(t_individual)
                onto_pizza.add_topping(t_individual)

    def classify_pizza(self, data):
        with self.onto:
            print('Assessing pizza...')
            print('Adding pizza to ontology...')
            onto_pizza = self.oao.add_pizza_to_ontology(data)
            pizza_result = self.create_pizza(onto_pizza, data)
            print('Pizza added to ontology.')
            print('Running classification...')
            self.ontology_service.sync()
            classes = onto_pizza.get_class_list()
            print(classes)
            print('Assessment completed.')
        
        
                
        return classes

    