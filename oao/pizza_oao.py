from owlready2 import *

class PizzaOao:
    def __init__(self, ontology_service): 
        self.ontology_service = ontology_service
        self.onto = self.ontology_service.get_onto()
        self.build_pizza_class_def()

    def build_pizza_class_def(self):
        with self.onto:
            class Pizza(Thing): 
                def add_topping(self, topping):
                    self.hasTopping.append(topping)
