from owlready2 import *

class Oao:
    def __init__(self, ontology_service):
        self.ontology_service = ontology_service
        self.onto = self.ontology_service.get_onto()
        self.build_thing_class_def()

    def build_thing_class_def(self):
        with owl:
            class Thing(owl.Thing):            
                def get_class_list(self):
                    print(self.is_a)
                    classes = set()
                    for cl in self.is_a:
                        for a in cl.ancestors():
                            if a.name != 'Thing': classes.add(a.name)
                    return list(classes)

    def add_pizza_to_ontology(self, data):
        pizza = self.onto.Pizza(data.name)
        # concept.name = data.name
        return pizza

    def get_topping_instance(self, topping):
        for t in self.onto.Topping.instances():
            if t.name == topping:
                return t