# README #

### Code demo for Dr Frey's precision medicine class  ###

* Mixing web APIs with OWL ontologies
* Version 1.0

### How do I get set up? ###

* clone the repo
* create a virtualenv i.e. 1. pip install virtualenv and 2 virtualenv . (do not forget the period)
* pipenv install
* uvicorn main:app --reload
* point your browser to localhost:8000/docs

### Who do I talk to? ###

* Jordon Ritchie
* ritchiej@musc.edu